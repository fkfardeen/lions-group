$(function(){
	
	var fiftySlider = {
		active: 0,
		$el: $('#fifty-fifty-slider-stack-id'),
		$slides: $('#fifty-fifty-slider-stack-id .ffs-slide'),
		$navCount: $('#fifty-fifty-slider-stack-id .ffs-nav-count'),
		numSlides: $('#fifty-fifty-slider-stack-id .ffs-slide').length,
		initialize: function(){
			this.setHeight();
			this.updateCount();
			this.$navCount.find('.ffs-total')
				.html(this.numSlides)
		},
		updateCount: function(){
			this.$navCount.find('.ffs-current')
				.html(this.active+1)
		},
		setHeight: function(){
			var elementHeights = this.$slides.map(function() {
				return $(this).height();
			}).get();

			var maxHeight = Math.max.apply(null, elementHeights);
			
			this.$el.css('height', maxHeight).addClass('height-set');
			this.$slides.css('bottom', '');
		},
		nextSlide: function(){
			this.$el.removeClass('slide-start');
			if(this.active+1 < this.numSlides){
				$(this.$slides[this.active+1]).addClass('active');
				if(this.active+1 == this.numSlides-1){
					this.$el.addClass('slide-end');
				}
				this.active++;
			}
			this.updateCount();
			this.setHeight();
		},
		prevSlide: function(){
			this.$el.removeClass('slide-end');
			if(this.active-1 >= 0){
				$(this.$slides[this.active]).removeClass('active the-one');
				$(this.$slides[this.active-1]).addClass('the-one')
				if(this.active-1 == 0){
					this.$el.addClass('slide-start');
				}
				this.active--;
			}
			this.updateCount();
			this.setHeight();
		}
	}

	$(window).resize(function(){
		fiftySlider.$slides.css('bottom', 'auto');
		fiftySlider.setHeight();
	});

	fiftySlider.initialize();
	
	$('#fifty-fifty-slider-stack-id a.next').click(function(){
		fiftySlider.nextSlide();
	})

	$('#fifty-fifty-slider-stack-id a.prev').click(function(){
		fiftySlider.prevSlide();
	})

});

$(function(){
	
	var sixtySlider = {
		active: 0,
		$el: $('#sixty-sixty-slider-stack-id'),
		$slides: $('#sixty-sixty-slider-stack-id .ffs-slide'),
		$navCount: $('#sixty-sixty-slider-stack-id .ffs-nav-count'),
		numSlides: $('#sixty-sixty-slider-stack-id .ffs-slide').length,
		initialize: function(){
			this.setHeight();
			this.updateCount();
			this.$navCount.find('.ffs-total')
				.html(this.numSlides)
		},
		updateCount: function(){
			this.$navCount.find('.ffs-current')
				.html(this.active+1)
		},
		setHeight: function(){
			var elementHeights = this.$slides.map(function() {
				return $(this).height();
			}).get();

			var maxHeight = Math.max.apply(null, elementHeights);
			
			this.$el.css('height', maxHeight).addClass('height-set');
			this.$slides.css('bottom', '');
		},
		nextSlide: function(){
			this.$el.removeClass('slide-start');
			if(this.active+1 < this.numSlides){
				$(this.$slides[this.active+1]).addClass('active');
				if(this.active+1 == this.numSlides-1){
					this.$el.addClass('slide-end');
				}
				this.active++;
			}
			this.updateCount();
			this.setHeight();
		},
		prevSlide: function(){
			this.$el.removeClass('slide-end');
			if(this.active-1 >= 0){
				$(this.$slides[this.active]).removeClass('active the-one');
				$(this.$slides[this.active-1]).addClass('the-one')
				if(this.active-1 == 0){
					this.$el.addClass('slide-start');
				}
				this.active--;
			}
			this.updateCount();
			this.setHeight();
		}
	}

	$(window).resize(function(){
		sixtySlider.$slides.css('bottom', 'auto');
		sixtySlider.setHeight();
	});

	sixtySlider.initialize();
	
	$('#sixty-sixty-slider-stack-id a.next').click(function(){
		sixtySlider.nextSlide();
	})

	$('#sixty-sixty-slider-stack-id a.prev').click(function(){
		sixtySlider.prevSlide();
	})

});

$(function(){
	
	var seventySlider = {
		active: 0,
		$el: $('#seventy-seventy-slider-stack-id'),
		$slides: $('#seventy-seventy-slider-stack-id .ffs-slide'),
		$navCount: $('#seventy-seventy-slider-stack-id .ffs-nav-count'),
		numSlides: $('#seventy-seventy-slider-stack-id .ffs-slide').length,
		initialize: function(){
			this.setHeight();
			this.updateCount();
			this.$navCount.find('.ffs-total')
				.html(this.numSlides)
		},
		updateCount: function(){
			this.$navCount.find('.ffs-current')
				.html(this.active+1)
		},
		setHeight: function(){
			var elementHeights = this.$slides.map(function() {
				return $(this).height();
			}).get();

			var maxHeight = Math.max.apply(null, elementHeights);
			
			this.$el.css('height', maxHeight).addClass('height-set');
			this.$slides.css('bottom', '');
		},
		nextSlide: function(){
			this.$el.removeClass('slide-start');
			if(this.active+1 < this.numSlides){
				$(this.$slides[this.active+1]).addClass('active');
				if(this.active+1 == this.numSlides-1){
					this.$el.addClass('slide-end');
				}
				this.active++;
			}
			this.updateCount();
			this.setHeight();
		},
		prevSlide: function(){
			this.$el.removeClass('slide-end');
			if(this.active-1 >= 0){
				$(this.$slides[this.active]).removeClass('active the-one');
				$(this.$slides[this.active-1]).addClass('the-one')
				if(this.active-1 == 0){
					this.$el.addClass('slide-start');
				}
				this.active--;
			}
			this.updateCount();
			this.setHeight();
		}
	}

	$(window).resize(function(){
		seventySlider.$slides.css('bottom', 'auto');
		seventySlider.setHeight();
	});

	seventySlider.initialize();
	
	$('#seventy-seventy-slider-stack-id a.next').click(function(){
		seventySlider.nextSlide();
	})

	$('#seventy-seventy-slider-stack-id a.prev').click(function(){
		seventySlider.prevSlide();
	})

});

$(function(){
	
	var eightySlider = {
		active: 0,
		$el: $('#eighty-eighty-slider-stack-id'),
		$slides: $('#eighty-eighty-slider-stack-id .ffs-slide'),
		$navCount: $('#eighty-eighty-slider-stack-id .ffs-nav-count'),
		numSlides: $('#eighty-eighty-slider-stack-id .ffs-slide').length,
		initialize: function(){
			this.setHeight();
			this.updateCount();
			this.$navCount.find('.ffs-total')
				.html(this.numSlides)
		},
		updateCount: function(){
			this.$navCount.find('.ffs-current')
				.html(this.active+1)
		},
		setHeight: function(){
			var elementHeights = this.$slides.map(function() {
				return $(this).height();
			}).get();

			var maxHeight = Math.max.apply(null, elementHeights);
			
			this.$el.css('height', maxHeight).addClass('height-set');
			this.$slides.css('bottom', '');
		},
		nextSlide: function(){
			this.$el.removeClass('slide-start');
			if(this.active+1 < this.numSlides){
				$(this.$slides[this.active+1]).addClass('active');
				if(this.active+1 == this.numSlides-1){
					this.$el.addClass('slide-end');
				}
				this.active++;
			}
			this.updateCount();
			this.setHeight();
		},
		prevSlide: function(){
			this.$el.removeClass('slide-end');
			if(this.active-1 >= 0){
				$(this.$slides[this.active]).removeClass('active the-one');
				$(this.$slides[this.active-1]).addClass('the-one')
				if(this.active-1 == 0){
					this.$el.addClass('slide-start');
				}
				this.active--;
			}
			this.updateCount();
			this.setHeight();
		}
	}

	$(window).resize(function(){
		eightySlider.$slides.css('bottom', 'auto');
		eightySlider.setHeight();
	});

	eightySlider.initialize();
	
	$('#eighty-eighty-slider-stack-id a.next').click(function(){
		eightySlider.nextSlide();
	})

	$('#eighty-eighty-slider-stack-id a.prev').click(function(){
		eightySlider.prevSlide();
	})

});

$(function(){
	
	var ninetySlider = {
		active: 0,
		$el: $('#ninety-ninety-slider-stack-id'),
		$slides: $('#ninety-ninety-slider-stack-id .ffs-slide'),
		$navCount: $('#ninety-ninety-slider-stack-id .ffs-nav-count'),
		numSlides: $('#ninety-ninety-slider-stack-id .ffs-slide').length,
		initialize: function(){
			this.setHeight();
			this.updateCount();
			this.$navCount.find('.ffs-total')
				.html(this.numSlides)
		},
		updateCount: function(){
			this.$navCount.find('.ffs-current')
				.html(this.active+1)
		},
		setHeight: function(){
			var elementHeights = this.$slides.map(function() {
				return $(this).height();
			}).get();

			var maxHeight = Math.max.apply(null, elementHeights);
			
			this.$el.css('height', maxHeight).addClass('height-set');
			this.$slides.css('bottom', '');
		},
		nextSlide: function(){
			this.$el.removeClass('slide-start');
			if(this.active+1 < this.numSlides){
				$(this.$slides[this.active+1]).addClass('active');
				if(this.active+1 == this.numSlides-1){
					this.$el.addClass('slide-end');
				}
				this.active++;
			}
			this.updateCount();
			this.setHeight();
		},
		prevSlide: function(){
			this.$el.removeClass('slide-end');
			if(this.active-1 >= 0){
				$(this.$slides[this.active]).removeClass('active the-one');
				$(this.$slides[this.active-1]).addClass('the-one')
				if(this.active-1 == 0){
					this.$el.addClass('slide-start');
				}
				this.active--;
			}
			this.updateCount();
			this.setHeight();
		}
	}

	$(window).resize(function(){
		ninetySlider.$slides.css('bottom', 'auto');
		ninetySlider.setHeight();
	});

	ninetySlider.initialize();
	
	$('#ninety-ninety-slider-stack-id a.next').click(function(){
		ninetySlider.nextSlide();
	})

	$('#ninety-ninety-slider-stack-id a.prev').click(function(){
		ninetySlider.prevSlide();
	})

});
// work in progress - needs some refactoring and will drop JQuery i promise :)
var instance = $(".hs__wrapper");
$.each( instance, function(key, value) {
    
  var arrows = $(instance[key]).find(".arrow"),
      prevArrow = arrows.filter('.arrow-prev'),
      nextArrow = arrows.filter('.arrow-next'),
      box = $(instance[key]).find(".hs"), 
      x = 0,
      mx = 0,
      maxScrollWidth = box[0].scrollWidth - (box[0].clientWidth / 2) - (box.width() / 2);

  $(arrows).on('click', function() {
      
    if ($(this).hasClass("arrow-next")) {
      x = ((box.width() / 2)) + box.scrollLeft() - 10;
      box.animate({
        scrollLeft: x,
      })
    } else {
      x = ((box.width() / 2)) - box.scrollLeft() -10;
      box.animate({
        scrollLeft: -x,
      })
    }
      
  });
    
  $(box).on({
    mousemove: function(e) {
      var mx2 = e.pageX - this.offsetLeft;
      if(mx) this.scrollLeft = this.sx + mx - mx2;
    },
    mousedown: function(e) {
      this.sx = this.scrollLeft;
      mx = e.pageX - this.offsetLeft;
    },
    scroll: function() {
      toggleArrows();
    }
  });

  $(document).on("mouseup", function(){
    mx = 0;
  });
  
  function toggleArrows() {
    if(box.scrollLeft() > maxScrollWidth - 10) {
        // disable next button when right end has reached 
        nextArrow.addClass('disabled');
      } else if(box.scrollLeft() < 10) {
        // disable prev button when left end has reached 
        prevArrow.addClass('disabled')
      } else{
        // both are enabled
        nextArrow.removeClass('disabled');
        prevArrow.removeClass('disabled');
      }
  }
  
});
$(".hover").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );