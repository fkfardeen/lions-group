$(document).ready(function () {
  $(".news_media_tab_data").click(function () {
    var title = $(this).attr("data-target");
    var id = $(this).attr("data-id");
    var year = $("#year-filter").find(":selected").text();
    var view_more = 0;
    $.ajax({
      url: site_root_url + "ajax/getNewsMediaData",
      type: "POST",
      data: { title: title, id: id, year: year, more: view_more },
      success: function (data) {
        $("#all_news_media_data_display").html(data);
        $("#view_all_news_media_data").html("");
      },
      error: function (data) {
        $("#all_news_media_data_display").html("No Data");
      },
    });
  });
  $("#media-tab-nav").change(function () {
    var title = $("option:selected", this).attr("data-target");
    var id = $("option:selected", this).attr("data-id");
    var year = $("#year-filter").find(":selected").text();
    var view_more = 0;
    $.ajax({
      url: site_root_url + "ajax/getNewsMediaData",
      type: "POST",
      data: { title: title, id: id, year: year, more: view_more },
      success: function (data) {
        $("#all_news_media_data_display").html(data);
        $("#view_all_news_media_data").html("");
      },
      error: function (data) {
        $("#all_news_media_data_display").html("No Data");
      },
    });
  });
  $("#year-filter").change(function () {
    if ($(window).width() <= 767) {
      var title = $("#media-tab-nav").find(":selected").attr("data-target");
      var id = $("#media-tab-nav").find(":selected").attr("data-id");
      var year = $("#year-filter").find(":selected").text();
    } else {
      var title = $(".tab-navigation li a.active").attr("data-target");
      var id = $(".tab-navigation li a.active").attr("data-id");
      var year = $("#year-filter").find(":selected").text();
      var view_more = 0;
    }
    $.ajax({
      url: site_root_url + "ajax/getNewsMediaData",
      type: "POST",
      data: { title: title, id: id, year: year, more: view_more },
      success: function (data) {
        $("#all_news_media_data_display").html(data);
        $("#view_all_news_media_data").html("");
      },
      error: function (data) {
        $("#all_news_media_data_display").html("No Data");
      },
    });
  });
  $("#view_all_news_media").click(function () {
    var title = $(".tab-navigation li a.active").attr("data-target");
    var id = $(".tab-navigation li a.active").attr("data-id");
    var year = $("#year-filter").find(":selected").text();
    var abc1 = $(this).attr("data-target");
    var view_more = $("#input_news_media").val();
    $.ajax({
      url: site_root_url + "ajax/getNewsMediaData",
      type: "POST",
      data: { title: title, id: id, year: year, more: view_more },
      success: function (data) {
        $("#view_all_news_media_data").append(data);
      },
      error: function (data) {
        $("#view_all_news_media_data").html("No Data");
      },
    });
  });
  $("#view_all_investor_report").click(function () {
    var view_more = "1";
    $.ajax({
      url: site_root_url + "ajax/getInvestorData",
      type: "POST",
      data: { more: view_more },
      success: function (data) {
        $("#view_all_investor_data").html(data);
        $("#view_all_investor_report").hide();
        $("#load_Less_investor_report_l").show();
      },
      error: function (data) {
        $("#view_all_investor_data").html("No Data");
      },
    });
  });
  $("#load_Less_investor_report_l").click(function () {
    $(".load_less_hide").hide();
    $("#view_all_investor_report").show();
    $("#load_Less_investor_report_l").hide();
  });
  $(".schema_data_desktop").click(function () {
    var title = $(this).attr("data-target");
    var id = $(this).attr("data-id");
    $.ajax({
      url: site_root_url + "ajax/getOfferSchemaData",
      type: "POST",
      data: { title: title, id: id },
      success: function (data) {
        $("#tabs-content").html(data);
        $(".project-slider")
          .not(".slick-initialized")
          .slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            cssEase: "ease-in-out",
            touchThreshold: 100,
            responsive: [
              { breakpoint: 1200, settings: { slidesToShow: 3 } },
              { breakpoint: 1024, settings: { slidesToShow: 3 } },
              { breakpoint: 769, settings: { slidesToShow: 2 } },
              { breakpoint: 767, settings: { slidesToShow: 2, dots: true } },
              { breakpoint: 480, settings: { slidesToShow: 1, dots: true } },
            ],
          });
      },
      error: function (data) {
        $("#tabs-content").html("No Data");
      },
    });
  });
  if ($(window).width() <= 767) {
    $(".schema_data_mobiles").change(function () {
      var title = $("#press-tab-nav").find(":selected").attr("data-target");
      var id = $("#press-tab-nav").find(":selected").attr("data-id");
      var values = $("#press-tab-nav").find(":selected").val();
      $.ajax({
        url: site_root_url + "ajax/getOfferSchemaData",
        type: "POST",
        data: { title: title, id: id },
        success: function (data) {
          $("#tabs-content").html(data);
          $(".project-slider")
            .not(".slick-initialized")
            .slick({
              infinite: false,
              slidesToShow: 4,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
              cssEase: "ease-in-out",
              touchThreshold: 100,
              responsive: [
                { breakpoint: 1200, settings: { slidesToShow: 3 } },
                { breakpoint: 1024, settings: { slidesToShow: 3 } },
                { breakpoint: 769, settings: { slidesToShow: 2 } },
                { breakpoint: 767, settings: { slidesToShow: 2, dots: true } },
                { breakpoint: 480, settings: { slidesToShow: 1, dots: true } },
              ],
            });
        },
        error: function (data) {
          $("#tabs-content").html("No Data");
        },
      });
    });
  }
  $("#download_brochure_form").validate({
    rules: {
      fname: { required: true, letterswithbasicpunc: true },
      lname: { required: true, letterswithbasicpunc: true },
      email: { required: true, email: true },
      designation: { required: true },
      organ: { required: true },
    },
    messages: {
      fname: { required: "This field is required" },
      lname: { required: "This field is required" },
      email: { required: "This field is required" },
      designation: { required: "This field is required" },
      organ: { required: "This field is required" },
    },
  });
  $(".download_brochure_submit").click(function () {
    var formdata = $("#download_brochure_form").serialize();
    var isvalidate_enq = $("#download_brochure_form").valid();
    if (isvalidate_enq) {
      $.ajax({
        url: site_root_url + "ajax/download_brochure",
        datatype: "text",
        type: "POST",
        data: $("#download_brochure_form").serialize(),
        success: function (data) {
          var res = JSON.parse(data);
          if (res.status == 200) {
            window.open(res.file_name, "_blank");
          }
          document.getElementById("download_brochure_form").reset();
        },
        error: function (data) {},
      });
    }
  });
  $(".policy_download_pdf").click(function () {
    var pdf_id = $(this).attr("data-id");
    var pdf_name = $(this).attr("data-name");
    $("#policy_pdf_id").val(pdf_id);
    $("#pdf_hidden_name").val(pdf_name);
  });
  $("#policy_download_form").validate({
    rules: {
      fname: { required: true, letterswithbasicpunc: true },
      lname: { required: true, letterswithbasicpunc: true },
      email: { required: true, email: true },
      designation: { required: true },
      organ: { required: true },
    },
    messages: {
      fname: { required: "This field is required" },
      lname: { required: "This field is required" },
      email: { required: "This field is required" },
      designation: { required: "This field is required" },
      organ: { required: "This field is required" },
    },
  });
  $(".policy_download_submit").click(function () {
    var formdata = $("#policy_download_form").serialize();
    var isvalidate_enq = $("#policy_download_form").valid();
    if (isvalidate_enq) {
      $.ajax({
        url: site_root_url + "ajax/policy_download",
        datatype: "text",
        type: "POST",
        data: $("#policy_download_form").serialize(),
        success: function (data) {
          var res = JSON.parse(data);
          if (res.status == 200) {
            window.open(res.file_name, "_blank");
          }
          document.getElementById("policy_download_form").reset();
        },
        error: function (data) {},
      });
    }
  });
  var home_career = "";
  var juicer_social = "";
  var VIDEO_CALLED = 0;
  var JUICER_CALLED = 0;
  var home_career = $("#career_video_data").attr("data-anchor");
  if (home_career) {
    $(document).scroll(function () {
      var WINDOW_TOP = $(window).scrollTop();
      var DIV_TOP = $("#career_video_data").offset().top - 500;
      if (WINDOW_TOP >= DIV_TOP && VIDEO_CALLED == 0) {
        VIDEO_CALLED = 1;
        var videoLink = $("#career_video_link").val();
        video_data =
          "<iframe class='iframe-career-vid' src='" +
          videoLink +
          "' data-frame='" +
          videoLink +
          "'  allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen=''></iframe>";
        $("#video-career-home").html(video_data);
      }
    });
  }
  var juicer_social = $("#home-juicer-social").attr("data-anchor");
  if (juicer_social) {
    $(document).scroll(function () {
      var WINDOW_TOP = $(window).scrollTop();
      var DIV_TOP = $("#home-juicer-social").offset().top - 500;
      if (WINDOW_TOP >= DIV_TOP && JUICER_CALLED == 0) {
        JUICER_CALLED = 1;
        juicer_data =
          "<iframe id='tata-projects-juicer' src='https://www.juicer.io/api/feeds/tata-projects/iframe' data-frame='https://www.juicer.io/api/feeds/tata-projects/iframe'  class='juice-height' style='display:block;margin:0 auto;'></iframe>";
        $("#juicer-social-data").html(juicer_data);
      }
    });
  }
});
