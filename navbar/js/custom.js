var videoDeskBanner = $(".slider-media.in-desktop video source").attr(
    "data-video"
  );
  var juicerprojects = $("#lions-projects-juicer").attr("data-frame");
  
  var careerVideo = $(".career-video-wrapper iframe").attr("data-frame");
  
  $(window).on("load", function() {
  
         // $('#jamshed-lions').modal('show');
     
  
    if ($(".slider-media.in-desktop video").length) {
      $(".slider-media.in-desktop video source").attr("src", videoDeskBanner);
      //$(".slider-media.in-mobile video source").attr("src", videoXsBanner);
      $(".slider-media.in-desktop video")[0].load();
    }
  
    $("#lions-projects-juicer").attr("src", juicerprojects);
    $(".career-video-wrapper iframe").attr("src", careerVideo);
    
  });
  
  $(document).ready(function() {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iphone)/);
    if (agentID) {
      $("body").addClass("modal-open-ios");
      //console.log('iPhone');
      $(function() {
        var $window = $(window),
          $body = $("body"),
          $modal = $(".modal"),
          scrollDistance = 0;
  
        $modal.on("show.bs.modal", function() {
          // Get the scroll distance at the time the modal was opened
          scrollDistance = $window.scrollTop();
  
          // Pull the top of the body up by that amount
          $body.css("top", scrollDistance * -1);
        });
        $modal.on("hidden.bs.modal", function() {
          // Remove the negative top value on the body
          $body.css("top", "");
  
          // Set the window's scroll position back to what it was before the modal was opened
          $window.scrollTop(scrollDistance);
        });
      });
    }
  
    $(document).on("mouseover", ".mega-menu .mega-inner-wraper li a", function() {
      var navImg = $(this).attr("data-img");
      var imgAlt = $(this).attr("data-alt");
      $(this)
        .parents(".mega-menu")
        .find(".inner-right img")
        .attr("src", navImg);
  
      $(this)
        .parents(".mega-menu")
        .find(".inner-right img")
        .attr("alt", imgAlt);
    });
  
    $(document).on("click", "#ham", function() {
      $("body").toggleClass("nav-is-toggled");
    });
    if ($(window).width() > 768) {
      var lastScrollTop = 0;
      $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
          $(".desktop-menu").removeClass("fixed-header");
        } else {
          $(".desktop-menu").addClass("fixed-header");
          $(".anchor-block").removeClass("fixed");
        }
        lastScrollTop = st;
        if (st <= 0) {
          $(".desktop-menu").removeClass("fixed-header");
        }
      });
    } else {
      var lastScrollTop = 0;
      $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
          // $(".desktop-menu").addClass("fixed-header");
          // $(".mobile-menu").show();
        } else {
          // $(".desktop-menu").removeClass("fixed-header");
          // $(".mobile-menu").hide();
        }
        lastScrollTop = st;
        if (st <= 0) {
          // $(".desktop-menu").removeClass("fixed-header");
          // $(".mobile-menu").show();
        }
      });
    }
  
    $(window).resize(function() {
      $(".sources-slider, .project-slider").each(function() {
        // Cache the highest
        var highestBox = 0;
        // Select and loop the elements you want to equalise
        $(".sources-item, .project-item", this).each(function() {
          // If this box is higher than the cached highest then store it
          if ($(this).height() > highestBox) {
            highestBox = $(this).height();
          }
        });
        // Set the height of all those children to whichever was highest
        $(".sources-item, .project-item", this).height(highestBox);
      });
    });
    $(window).scroll(function() {
      if ($(this).scrollTop() >= 600) {
        // If page is scrolled more than 50px
        $(".anchor-block").addClass("fixed"); // Fade in the arrow
      } else {
        $(".anchor-block").removeClass("fixed"); // Else fade out the arrow
      }
    });
  
    //homepage slider js start here
    $(".main-slider").slick({
      autoplay: false,
      cssEase: "linear",
      pauseOnHover: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    $(".main-slider").on("init", function(event, slick, currentSlide, nextSlide) {
      nextInfo();
    });
  
    $(".other-info .next").click(function() {
      $(".main-slider").slick("slickNext");
    });
    $(".other-info .prev").click(function() {
      $(".main-slider").slick("slickPrev");
    });
    //var video = $('.main-slider .slick-active').find('video').get(0).play();
    $(".main-slider").on("beforeChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      $(".main-slider video").each(function() {
        $(this)
          .get(0)
          .pause();
      });
    });
    // On slide chnage, play a video inside the current slide
    $(".main-slider").on("init afterChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      if ($(".slick-current").find("video").length !== 0) {
        $(".main-slider .slick-current video")[0].play();
      }
    });
    $(".main-slider").on("init afterChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      nextInfo();
    });
    //homepage slider js end here
    //project highlight slider js start here
    $(".highlights-slider").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      lazyLoad: "anticipated",
      arrows: false,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    $(".highlights-wrapper .next").click(function() {
      $(".highlights-slider").slick("slickNext");
    });
    $(".highlights-wrapper .prev").click(function() {
      $(".highlights-slider").slick("slickPrev");
    });
    //project highlight slider js end here
    //sector slider js strt here
    $(".sector-slider").slick({
      lazyLoad: "anticipated",
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      cssEase: "ease-in-out",
      touchThreshold: 100,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            dots: true
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            dots: true
          }
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  
    $(".methodology-slider").slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 9999,
          settings: "unslick"
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
          }
        }
      ]
    });
  
    $(".slick-slide:nth-child(1) a.copy").addClass("sector-active");
    var sectorActive = $(".sectors-wrapper .slick-active a.copy").data(
      "background"
    );
    $(".sectors-wrapper").css("background-image", "url(" + sectorActive + ")");
    $(".sector-item a.copy").hover(function() {
      $(".sector-item a.copy").removeClass("sector-active");
      $(this).addClass("sector-active");
      var backgroundPicture = $(this).data("background");
      $(".sectors-wrapper").css(
        "background-image",
        "url(" + backgroundPicture + ")"
      );
    });
    //sector slider js end here
    //homepage offering js start here
    $.fn.accordion = function() {
      const trigger = $(this).find(".accordion-trigger");
      const collapse = $(this).find(".accordion-collapse");
      $(trigger)
        .first()
        .addClass("accordion-open");
      $(collapse)
        .first()
        .show();
      $(trigger).each(function() {
        $(this).on("click", function() {
          $(this)
            .siblings(".accordion-collapse")
            .slideToggle();
          $(this).toggleClass("accordion-open");
          $(this)
            .parent()
            .siblings(".accordion-item")
            .find(".accordion-trigger")
            .removeClass("accordion-open");
          $(this)
            .parent()
            .siblings(".accordion-item")
            .find(".accordion-collapse")
            .slideUp();
        });
      });
    };
    $(".accordion").accordion();
    // Show the first tab and hide the rest
    $("#tabs-nav li:first-child a").addClass("active");
    $(".tab-content").hide();
    $(".tab-content:first").show();
    // Click function
    $("#tabs-nav li a").click(function() {
      $("#tabs-nav li a").removeClass("active");
      $(this).addClass("active");
      $(".tab-content").hide();
      var activeTab = $(this).data("val");
      // $(".press-slider").slick();
  
      $(activeTab).fadeIn();
      $(".press-slider1").slick("refresh");
      $(".press-slider2").slick("refresh");
      $(".press-slider3").slick("refresh");
  
      // $(".project-slider").slick("refresh");
  
      return false;
    });
    $(".select-change").click(function() {
      $("#press-tab-nav")
        .val($(this).data("val"))
        .trigger("change");
    });
    // $('.tab-navigation li:first-child a').addClass('active');
    $(".tab-navigation li a").click(function() {
      $(".tab-navigation li a").removeClass("active");
      $(this).addClass("active");
    });
    // $('.media-select').click(function() {
    //     $('#media-tab-nav').val($(this).data('val')).trigger('change');
    //console.log('changed');
    // });
    $(".mediapress-tab").click(function() {
      // alert('mediapress');
    });
    $(".mediaawards-tab").click(function() {
      // alert('mediaawards');
    });
    $(".medianews-tab").click(function() {
      // alert('medianews');
    });
    //homepage offering js end here
    // homepage investor relation js start here
    $(".investor-slider").slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    // homepage investor relation js end here
    // press slider js start here
    setTimeout(function() {
      $(".press-slider").each(function() {
        var pressActiveOnload = $(".slick-active .tpl-copy", this).data(
          "desktopimage"
        );
  
        var imgalt1 = $(".slick-active .tpl-copy", this).data("imgalt");
        //  console.log(pressActiveOnload);
        $(this)
          .parent(".press-slider-wrapper")
          .siblings(".tpl-image-block")
          .children("img")
          .attr("src", pressActiveOnload);
  
        $(this)
          .parent(".press-slider-wrapper")
          .siblings(".tpl-image-block")
          .children("img")
          .attr("alt", imgalt1);
      });
    }, 500);
    $(".press-slider1").slick({
      // lazyLoad: "anticipated",
      autoplay: false,
      cssEase: "ease-in-out",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      // adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            dots: true
          }
        }
      ]
    });
  
    // $(".tpl_img_slder").slick({
    //   autoplay: false,
    //   cssEase: "ease-in-out",
    //   pauseOnHover: true,
    //   autoplaySpeed: 5000,
    //   infinite: true,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   arrows: false,
    //   asNavFor: ".press-slider1",
    //   adaptiveHeight: true,
    //   responsive: [
    //     {
    //       breakpoint: 767,
    //       settings: {
    //         dots: false
    //       }
    //     },
    //     {
    //       breakpoint: 480,
    //       settings: {
    //         dots: false
    //       }
    //     }
    //   ]
    // });
  
    if ($(window).width() <= 767) {
      $(".desktop-menu").addClass("fixed-header");
      $("#press-tab-nav").change(function() {
        var dropdownSelected = $(this)
          .find(":selected")
          .val();
        // console.log(dropdownSelected);
        $(".tab-content").hide();
        $("#" + dropdownSelected).show();
        $(".press-slider1").slick("refresh");
        $(".press-slider2").slick("refresh");
        $(".press-slider3").slick("refresh");
        // console.log(liSelected);
      });
      $("#media-tab-nav").change(function() {
        var dropdownSelected1 = $(this)
          .find(":selected")
          .val();
        $(".tab-content").hide();
        $("#" + dropdownSelected1).show();
      });
    }
    //  if ($(window).width() <= 767) {
    //   $("#press-tab-nav").change(function() {
    //     var dropdownSelected = $(this)
    //       .find(":selected")
    //       .val();
    //       alert(dropdownSelected);
    //     // console.log(dropdownSelected);
    //     $("#tabs-nav li a").each(function() {
    //       var liSelected = $(this).data("val");
    //       if (liSelected == dropdownSelected) {
    //         // alert('theres a match')
    //         $("#tabs-nav li a").removeClass("active");
    //         $(this).addClass("active");
    //         $(".tab-content").hide();
    //         var activeTab = $(this).attr("href");
    //         // $(".press-slider").slick();
    //         $(activeTab).fadeIn();
    //         $(".press-slider1").slick("refresh");
    //         $(".press-slider2").slick("refresh");
    //         $(".press-slider3").slick("refresh");
    //         return false;
    //         // $('#tabs-nav li a.active').trigger("click");
    //       }
    //     });
    //     // console.log(liSelected);
    //   });
    //   $("#media-tab-nav").change(function() {
    //     var dropdownSelected = $(this)
    //       .find(":selected")
    //       .val();
    //     $(".tab-navigation li a").each(function() {
    //       var liSelected = $(this).data("val");
    //       if (liSelected == dropdownSelected) {
    //         $(".tab-navigation li a").removeClass("active");
    //         $(this).addClass("active");
    //         $(this).trigger("click");
    //       }
    //     });
    //   });
    // }
    if ($(window).width() <= 767) {
      $(".map-wrapper .container-custom").scrollLeft(250);
    }
    $(".tpl-block1 .next").click(function() {
      $(".press-slider1").slick("slickNext");
    });
    $(".tpl-block1 .prev").click(function() {
      $(".press-slider1").slick("slickPrev");
    });
    $(".tpl-block2 .next").click(function() {
      $(".press-slider2").slick("slickNext");
    });
    $(".tpl-block2 .prev").click(function() {
      $(".press-slider2").slick("slickPrev");
    });
    $(".tpl-block3 .next").click(function() {
      $(".press-slider3").slick("slickNext");
    });
    $(".tpl-block3 .prev").click(function() {
      $(".press-slider3").slick("slickPrev");
    });
    $(".press-slider1").on("afterChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      var pressActive = $(".slick-active .tpl-copy", this).data("desktopimage");
      // console.log(pressActive);
      $(this)
        .parent(".press-slider-wrapper")
        .siblings(".tpl-image-block")
        .children("img")
        .attr("src", pressActive);
  
      var imgalt1 = $(".slick-active .tpl-copy", this).data("imgalt");
  
      $(this)
        .parent(".press-slider-wrapper")
        .siblings(".tpl-image-block")
        .children("img")
        .attr("alt", imgalt1);
    });
    $(".press-slider2").on("afterChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      var pressActive = $(".slick-active .tpl-copy", this).data("desktopimage");
      //  console.log(pressActive);
      $(this)
        .parent(".press-slider-wrapper")
        .siblings(".tpl-image-block")
        .children("img")
        .attr("src", pressActive);
    });
    $(".press-slider3").on("afterChange", function(
      event,
      slick,
      currentSlide,
      nextSlide
    ) {
      var pressActive = $(".slick-active .tpl-copy", this).data("desktopimage");
      // console.log(pressActive);
      $(this)
        .parent(".press-slider-wrapper")
        .siblings(".tpl-image-block")
        .children("img")
        .attr("src", pressActive);
    });
    // press slider js end here
    //About us methodology slider strta here
    $(".looking-for-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //About us methodology slider end here
    //about us aource slider strt
    $(".sources-slider").slick({
      autoplay: true,
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //about us aource slider end
    //sector key areas slider start here
    $(".area-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      lazyLoad: "anticipated",
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    $(".industrial-area-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      lazyLoad: "anticipated",
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    $(".investment-area-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      lazyLoad: "anticipated",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //sector key areas slider end here
    //sector key project slider start here
    $(".project-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //sector key project slider end here
    //touching inner slider js start
    $(".feel-the-change-slider").slick({
      autoplay: true,
      cssEase: "linear",
      pauseOnHover: true,
      autoplaySpeed: 5000,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
    //touching inner slider js end
  
    $(".test-slider").slick(
   {
   dots: true,
   infinite: true,
   arrows:false,
   slidesToShow: 4,
   slidesToScroll: 1,
  responsive: [
       {
         breakpoint: 1024,
         settings: {
           slidesToShow: 2,
         }
       },
       {
         breakpoint: 800,
         settings: {
           slidesToShow: 2,
           dots: true
         }
       },
       {
         breakpoint: 767,
         settings: {
           slidesToShow: 1,
           dots: true
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1,
           dots: true
         }
       }
       // You can unslick at a given breakpoint now by adding:
       // settings: "unslick"
       // instead of a settings object
     ]
  
   });
    
    //sector contact address slider start here
    $(".address-slider").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      adaptiveHeight: true
    });
    $(".slider-navigation .next").click(function() {
      $(".address-slider").slick("slickNext");
    });
    $(".slider-navigation .prev").click(function() {
      $(".address-slider").slick("slickPrev");
    });
    $(".nav-footer p").click(function() {
      $(this)
        .parent(".nav")
        .toggleClass("open");
      $("html, body").animate(
        { scrollTop: jQuery(this).offset().top - 170 },
        1000
      );
    });
    $("#drop_success_button").click(function() {
      $("#get-touch-form").validate({
        errorElement: "span",
        rules: {
          username: {
            required: true,
            letterswithbasicpunc: true
          },
          useremail: {
            required: true,
            email: true
          },
          organisation: {
            required: true
          },
          phone: {
            required: true,
            number: true,
            minlength: 10,
            maxlength: 10
          },
          msg: {
            required: true
          },
          enquiry_nature: {
            required: true
          }
        },
        messages: {
          username: {
            required: "This field is required."
          },
          useremail: {
            required: "This field is required.",
            email: "Invalid Email-Id."
          },
          organisation: {
            required: "This field is required."
          },
          phone: {
            required: "Contact no. is required.",
            minlength: "10 digits only",
            maxlength: "10 digits only",
            number: "Only numbers are allowed"
          },
          msg: {
            required: "This field is required."
          },
          enquiry_nature: {
            required: "This field is required."
          }
        }
      });
      var isvalidate_enq = $("#get-touch-form").valid();
      if (isvalidate_enq) {
        $.ajax({
          url: site_root_url + "ajax/drop_us_line",
          type: "POST",
          data: $("#get-touch-form").serialize(),
          beforeSend: function() {
            $("#getwait_msg").html("Please Wait...");
          },
          success: function(data) {
            var response = JSON.parse(data);
            if (response.status == true) {
              document.getElementById("drop_success_button").disabled = true;
              console.log(response.msg);
              $("#getwait_msg").html("");
              $("#getform_msg").html(response.msg);
              document.getElementById("get-touch-form").reset();
              $(".error").css({ display: "block" });
              document.getElementById("drop_success_button").disabled = false;
              setTimeout(function() {
                $("#form_msg").html("");
                $("#getform_msg").html("");
              }, 3000);
              grecaptcha.reset();
            } else {
              console.log(response.msg);
              $("#getwait_msg").html("");
              $("#getform_msg_error").html(response.msg);
              document.getElementById("drop_success_button").disabled = false;
              $(".error").css({ display: "block" });
            }
          },
          error: function(data) {
            console.log(data);
          }
        });
      }
    });
    var headerHeight = $("header").outerHeight();
    window.addEventListener("hashchange", function() {
      window.scrollTo(window.scrollX, window.scrollY - headerHeight);
    });
  });
  
  function nextInfo() {
    var nextName = $(".slick-current")
      .find(".slider-item")
      .data("slider-name");
    var nextCount = $(".slick-current")
      .find(".slider-item")
      .data("slider-count");
    $(".next-slider-title").text(nextName);
    $(".next-slide-number").text(nextCount);
  }
  
  if ($(".child2-img").length) {
    // init controller
    var controller = new ScrollMagic.Controller({ vertical: false });
  
    // build tween for 1st dot
    var tween1 = TweenMax.to(".child2-img", 0.5, { width: "40vw" });
  
    // build scene
    var scene = new ScrollMagic.Scene({
      triggerElement: ".content-wrapper .child:nth-child(2)",
      triggerHook: 0.57,
      duration: "30%",
      reverse: true
    })
      .setTween(tween1)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  
    // build tween for 2nd dot
    var tween2 = TweenMax.to(".child3-img", 0.5, { width: "20vw" });
  
    // build scene
    var scene = new ScrollMagic.Scene({
      triggerElement: ".content-wrapper .child:nth-child(3)",
      triggerHook: 0.6,
      duration: "30%",
      reverse: true
    })
      .setTween(tween2)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  
    // build tween for 3rd dot
    var tween2 = TweenMax.to(".child4-img", 0.5, { width: "28vw" });
  
    // build scene
    var scene = new ScrollMagic.Scene({
      triggerElement: ".content-wrapper .child:nth-child(4)",
      triggerHook: 0.65,
      duration: "50%",
      reverse: true
    })
      .setTween(tween2)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  
    // build tween for dot
    var tweendot = TweenMax.to(".main-dot", 0.5, { background: "#fff" });
  
    // build scene
    var tweendotscene = new ScrollMagic.Scene({
      triggerElement: ".main-dot",
      triggerHook: 0.49,
      duration: "10px",
      reverse: true
    })
      .setTween(tweendot)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  }
  const spaceHolder = document.querySelector(".space-holder");
  const horizontal = document.querySelector(".horizontal");
  spaceHolder.style.height = `${calcDynamicHeight(horizontal)}px`;
  
  function calcDynamicHeight(ref) {
    const vw = window.innerWidth;
    const vh = window.innerHeight;
    const objectWidth = ref.scrollWidth;
    return objectWidth - vw + vh;
  }
  
  window.addEventListener("scroll", () => {
    const sticky = document.querySelector(".sticky");
    horizontal.style.transform = `translateX(-${sticky.offsetTop}px)`;
  });
  
  window.addEventListener("resize", () => {
    spaceHolder.style.height = `${calcDynamicHeight(horizontal)}px`;
  });
  
  $(window).on("load", function() {
    nextInfo();
    $(".grid").masonry({
      // options
      itemSelector: ".grid-item",
      percentPosition: true
    });
  
    // Select and loop the container element of the elements you want to equalise
    $(".sources-slider, .project-slider").each(function() {
      // Cache the highest
      var highestBox = 0;
      // Select and loop the elements you want to equalise
      $(".sources-item, .project-item", this).each(function() {
        // If this box is higher than the cached highest then store it
        if ($(this).height() > highestBox) {
          highestBox = $(this).height();
        }
      });
      // Set the height of all those children to whichever was highest
      $(".sources-item, .project-item", this).height(highestBox);
    });
  });
  //sector  contact address slider end here
  $(".location").on("mouseenter", function() {
    $(".location").removeClass("active");
    $(this).addClass("active");
  });
  $(".location").on("mouseleave", function() {
    $(".location").removeClass("active");
  });
  $(".map-wrapper .container-custom ").scrollLeft(370);
  $(".mega, .mega-menu").on("mouseenter", function() {
    $("body").addClass("mega-menu-active");
    $("body").css("margin-right", "6px");
    $(".desktop-menu").addClass("fixed-header-mega");
  });
  $(".mega, .mega-menu").on("mouseleave", function() {
    $("body").removeClass("mega-menu-active");
    $("body").css("margin-right", "0px");
    $(".desktop-menu").removeClass("fixed-header-mega");
  });
  
  $("html").easeScroll();
  $(".acc-btn").click(function() {
    if (
      $(this)
        .next()
        .is(":hidden")
    ) {
      $(".acc-content").slideUp("selected");
      $(this)
        .prev(".acc-btn")
        .removeClass("active");
      $(this)
        .next()
        .slideDown("selected");
      $(this)
        .next(".acc-btn")
        .addClass("active");
    } else {
      $(this)
        .next()
        .slideUp("selected");
      $(this)
        .prev(".acc-btn")
        .addClass("active");
    }
  });
  //  $('#media-tab-nav').change(function() {
  //    var job = $('#media-tab-nav').val();
  //   if (job == 'mediapress-tab') {
  //  console.log('mediapress-tab');
  //       $('.mediapress-tab').trigger('click');
  //    }
  //    if (job == 'mediaawards-tab') {
  //       //  console.log('mediaawards-tab');
  //        $('.mediaawards-tab').trigger('click');
  //   }
  //  if (job == 'medianews-tab') {
  //  console.log('medianews-tab');
  //      $('.medianews-tab').trigger('click');
  //  }
  //  });
  
  $(".tab-navigation li a").click(function() {
    $(".tab-navigation li a").removeClass("active");
    $(this).addClass("active");
  });
  var elem = $(".career-video")[0];
  $(".video-play-button").click(function() {
    var fullscreen =
      elem.webkitRequestFullscreen ||
      elem.mozRequestFullScreen ||
      elem.msRequestFullscreen;
    fullscreen.call(elem); // bind the 'this' from the video object and instantiate the correct fullscreen method.
    elem.play();
    //$(this).fadeOut();
  });
  $(".scroll-sec a").click(function() {
    $("html, body").animate(
      {
        scrollTop: $("#project-highlight").offset().top
      },
      1000
    );
  });
  