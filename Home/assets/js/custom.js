// $('#recipeCarousel').carousel({
//     interval: 500
//   })
  
  $('.carousel .carousel-item').each(function(){
      var minPerSlide = 3;
      var next = $(this).next();
      if (!next.length) {
      next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      
      for (var i=0;i<minPerSlide;i++) {
          next=next.next();
          if (!next.length) {
              next = $(this).siblings(':first');
            }
          
          next.children(':first-child').clone().appendTo($(this));
        }
  });
  
  function slideControl($dir){
    $ClientWidth=$dir.parentNode.nextElementSibling.clientWidth;
    $SliderWidth=$dir.parentNode.nextElementSibling.scrollWidth;
    $slidesWidth=$dir.parentNode.nextElementSibling.children[0].clientWidth;
    console.log($slidesWidth)
    $slider=$dir.parentNode.nextElementSibling;
    $slidesWidth=($dir.classList.contains('ri-arrow-left-s-line'))? -$slidesWidth: $slidesWidth;
    $slider.scrollBy({
      left:$slidesWidth,
      behavior: 'smooth',
    })
  }
  function scrollSlides($scrollX){
    $leftArrow=$scrollX.previousElementSibling.children[0];
    $RightArrow=$scrollX.previousElementSibling.children[1];
    
    // for Left Arrow color change.
    $color=($scrollX.scrollLeft>10)? 'black':'unset';
    $leftArrow.setAttribute('style','color:'+$color)

    // for Right Arrow color change
    
    $color=(Math.floor($scrollX.scrollLeft+$scrollX.clientWidth)>$scrollX.scrollWidth-10)? 'unset':'black';
      $RightArrow.setAttribute('style','color:'+$color)
  }