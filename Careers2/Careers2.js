/*
** With Slick Slider Plugin : https://github.com/marvinhuebner/slick-animation
** And Slick Animation Plugin : https://github.com/marvinhuebner/slick-animation
*/

// Init slick slider + animation
$('.slider').slick({
    autoplay: true,
    speed: 800,
    lazyLoad: 'progressive',
    arrows: true,
    dots: false,
      prevArrow: '<div class="slick-nav prev-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
      nextArrow: '<div class="slick-nav next-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
  }).slickAnimation();
  
  
  
  $('.slick-nav').on('click touch', function(e) {
  
      e.preventDefault();
  
      var arrow = $(this);
  
      if(!arrow.hasClass('animate')) {
          arrow.addClass('animate');
          setTimeout(() => {
              arrow.removeClass('animate');
          }, 1600);
      }
  
  });

  $(document).ready(function(){
    $('.slider').slick({
        autoplay: false,
        loop:false
    });
  });

  setTimeout(function(){
    odometer.innerHTML = 1000;//ending number
    odometer1.innerHTML = 10;//ending number

    odometer2.innerHTML = 20;//ending number

}, 1000);//last argument is for time in milliseconds or seconds depending on the needsetTimeout(function(){
    $('.owl-carousel').owlCarousel({
        loop:false,
      stagePadding: 15,
        margin:10,
        nav:true,
      navText : ['<span class="uk-margin-small-right uk-icon" uk-icon="icon: chevron-left"></span>','<span class="uk-margin-small-left uk-icon" uk-icon="icon: chevron-right"></span>'],
        responsive:{
            0:{
                items:1
            },
            640:{
                items:2
            },
          960:{
                items:3
            },
            1200:{
                items:4
            }
        }
    })

    