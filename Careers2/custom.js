let activeIndex = 0;
let limit = 0;
let disabled = false;
let $stage;
let $controls;
let canvas = false;
const SPIN_FORWARD_CLASS = 'js-spin-fwd12345';
const SPIN_BACKWARD_CLASS = 'js-spin-bwd12345';
const DISABLE_TRANSITIONS_CLASS = 'js-transitions-disabled12345';
const SPIN_DUR = 1000;
const appendControls = () => {
  for (let i = 0; i < limit; i++) {
    $('.carousel__control12345').append(`<a href="#" data-index="${i}"></a>`);
  }
  let height = $('.carousel__control12345').children().last().outerWidth();
  $('.carousel__control12345').css('width', 30 + limit * height);
  $controls = $('.carousel__control12345').children();
  $controls.eq(activeIndex).addClass('active');
};
const setIndexes = () => {
  $('.spinner12345').children().each((i, el) => {
    $(el).attr('data-index', i);
    limit++;
  });
};
const duplicateSpinner = () => {
  const $el = $('.spinner12345').parent();
  const html = $('.spinner12345').parent().html();
  $el.append(html);
  $('.spinner12345').last().addClass('spinner--right12345');
  $('.spinner--right12345').removeClass('spinner--left');
};
const paintFaces = () => {
  $('.spinner__face12345').each((i, el) => {
    const $el = $(el);
    let color = $(el).attr('data-bg');
    $el.children().css('backgroundImage', `url(${getBase64PixelByColor(color)})`);
  });
};
const getBase64PixelByColor = hex => {
  if (!canvas) {
    canvas = document.createElement('canvas');
    canvas.height = 1;
    canvas.width = 1;
  }
  if (canvas.getContext) {
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = hex;
    ctx.fillRect(0, 0, 1, 1);
    return canvas.toDataURL();
  }
  return false;
};
const prepareDom = () => {
  setIndexes();
  paintFaces();
  duplicateSpinner();
  appendControls();
};
const spin = (inc = 1) => {
  if (disabled) return;
  if (!inc) return;
  activeIndex += inc;
  disabled = true;
  if (activeIndex >= limit) {
    activeIndex = 0;
  }
  if (activeIndex < 0) {
    activeIndex = limit - 1;
  }
  const $activeEls = $('.spinner__face12345.js-active');
  const $nextEls = $(`.spinner__face12345[data-index=${activeIndex}]`);
  $nextEls.addClass('js-next');
  if (inc > 0) {
    $stage.addClass(SPIN_FORWARD_CLASS);
  } else {
    $stage.addClass(SPIN_BACKWARD_CLASS);
  }
  $controls.removeClass('active');
  $controls.eq(activeIndex).addClass('active');
  setTimeout(() => {
    spinCallback(inc);
  }, SPIN_DUR, inc);
};
const spinCallback = inc => {
  $('.js-active').removeClass('js-active');
  $('.js-next').removeClass('js-next').addClass('js-active');
  $stage.
  addClass(DISABLE_TRANSITIONS_CLASS).
  removeClass(SPIN_FORWARD_CLASS).
  removeClass(SPIN_BACKWARD_CLASS);
  $('.js-active').each((i, el) => {
    const $el = $(el);
    $el.prependTo($el.parent());
  });
  setTimeout(() => {
    $stage.removeClass(DISABLE_TRANSITIONS_CLASS);
    disabled = false;
  }, 100);
};
const attachListeners = () => {
  document.onkeyup = e => {
    switch (e.keyCode) {
      case 38:
        spin(-1);
        break;
      case 40:
        spin(1);
        break;}
  };
  $controls.on('click', e => {
    e.preventDefault();
    if (disabled) return;
    const $el = $(e.target);
    const toIndex = parseInt($el.attr('data-index'), 10);
    spin(toIndex - activeIndex);
  });
};
const assignEls = () => {
  $stage = $('.carousel__stage12345');
};
const init = () => {
  assignEls();
  prepareDom();
  attachListeners();
};
$(() => {
  init();
});



function slideControl($dir){
  $ClientWidth=$dir.parentNode.nextElementSibling.clientWidth;
  $SliderWidth=$dir.parentNode.nextElementSibling.scrollWidth;
  $slidesWidth=$dir.parentNode.nextElementSibling.children[0].clientWidth;
  console.log($slidesWidth)
  $slider=$dir.parentNode.nextElementSibling;
  $slidesWidth=($dir.classList.contains('ri-arrow-left-s-line'))? -$slidesWidth: $slidesWidth;
  $slider.scrollBy({
    left:$slidesWidth,
    behavior: 'smooth',
  })
}
function scrollSlides($scrollX){
  $leftArrow=$scrollX.previousElementSibling.children[0];
  $RightArrow=$scrollX.previousElementSibling.children[1];
  
  // for Left Arrow color change.
  $color=($scrollX.scrollLeft>10)? 'white':'unset';
  $leftArrow.setAttribute('style','color:'+$color)

  // for Right Arrow color change
  
  $color=(Math.floor($scrollX.scrollLeft+$scrollX.clientWidth)>$scrollX.scrollWidth-10)? 'unset':'white';
    $RightArrow.setAttribute('style','color:'+$color)
}