jQuery( document ).ready(function( $ ) {


	"use strict";


    
        $(function() {
            $( "#tabs" ).tabs();
        });


        // Page loading animation

        $("#preloader").animate({
            'opacity': '0'
        }, 600, function(){
            setTimeout(function(){
                $("#preloader").css("visibility", "hidden").fadeOut();
            }, 300);
        });       

        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          var box = $('.header-text').height();
          var header = $('header').height();

          if (scroll >= box - header) {
            $("header").addClass("background-header");
          } else {
            $("header").removeClass("background-header");
          }
        });
		if ($('.owl-testimonials').length) {
            $('.owl-testimonials').owlCarousel({
                loop: true,
                nav: false,
                dots: true,
                items: 1,
                margin: 30,
                autoplay: false,
                smartSpeed: 700,
                autoplayTimeout: 6000,
                responsive: {
                    0: {
                        items: 1,
                        margin: 0
                    },
                    460: {
                        items: 1,
                        margin: 0
                    },
                    576: {
                        items: 2,
                        margin: 20
                    },
                    992: {
                        items: 2,
                        margin: 30
                    }
                }
            });
        }
        if ($('.owl-partners').length) {
            $('.owl-partners').owlCarousel({
                loop: true,
                nav: false,
                dots: true,
                items: 1,
                margin: 30,
                autoplay: false,
                smartSpeed: 700,
                autoplayTimeout: 6000,
                responsive: {
                    0: {
                        items: 1,
                        margin: 0
                    },
                    460: {
                        items: 1,
                        margin: 0
                    },
                    576: {
                        items: 2,
                        margin: 20
                    },
                    992: {
                        items: 4,
                        margin: 30
                    }
                }
            });
        }

        $(".Modern-Slider").slick({
            autoplay:true,
            autoplaySpeed:10000,
            speed:600,
            slidesToShow:1,
            slidesToScroll:1,
            pauseOnHover:false,
            dots:true,
            pauseOnDotsHover:true,
            cssEase:'linear',
           // fade:true,
            draggable:false,
            prevArrow:'<button class="PrevArrow"></button>',
            nextArrow:'<button class="NextArrow"></button>', 
        });

        function visible(partial) {
            var $t = partial,
                $w = jQuery(window),
                viewTop = $w.scrollTop(),
                viewBottom = viewTop + $w.height(),
                _top = $t.offset().top,
                _bottom = _top + $t.height(),
                compareTop = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop) && $t.is(':visible'));

        }

        $(window).scroll(function(){

          if(visible($('.count-digit')))
            {
              if($('.count-digit').hasClass('counter-loaded')) return;
              $('.count-digit').addClass('counter-loaded');
              
        $('.count-digit').each(function () {
          var $this = $(this);
          jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
            duration: 3000,
            easing: 'swing',
            step: function () {
              $this.text(Math.ceil(this.Counter));
            }
          });
        });
        }
    })
 
});
var wobbleElements = document.querySelectorAll('.wobble');

wobbleElements.forEach(function(el){
	el.addEventListener('mouseover', function(){
		
		if(!el.classList.contains('animating') && !el.classList.contains('mouseover')){
		
			el.classList.add('animating','mouseover');
		
			var letters = el.innerText.split('');
			
			setTimeout(function(){ el.classList.remove('animating'); }, (letters.length + 1) * 50);
			
			var animationName = el.dataset.animation;
			if(!animationName){ animationName = "jump"; }
		
			el.innerText = '';
		
			letters.forEach(function(letter){
				if(letter == " "){
					letter = "&nbsp;";
				}
				el.innerHTML += '<span class="letter">'+letter+'</span>';
			});
			
			var letterElements = el.querySelectorAll('.letter');
			letterElements.forEach(function(letter, i){
				setTimeout(function(){
					letter.classList.add(animationName);
				}, 50 * i);
			});
			
		}
		
	});
	
	el.addEventListener('mouseout', function(){				
		el.classList.remove('mouseover');
	});
});
function slideControl($dir){
    $ClientWidth=$dir.parentNode.nextElementSibling.clientWidth;
    $SliderWidth=$dir.parentNode.nextElementSibling.scrollWidth;
    $slidesWidth=$dir.parentNode.nextElementSibling.children[0].clientWidth;
    console.log($slidesWidth)
    $slider=$dir.parentNode.nextElementSibling;
    $slidesWidth=($dir.classList.contains('ri-arrow-left-s-line'))? -$slidesWidth: $slidesWidth;
    $slider.scrollBy({
      left:$slidesWidth,
      behavior: 'smooth',
    })
  }
  function scrollSlides($scrollX){
    $leftArrow=$scrollX.previousElementSibling.children[0];
    $RightArrow=$scrollX.previousElementSibling.children[1];
    
    // for Left Arrow color change.
    $color=($scrollX.scrollLeft>10)? 'white':'unset';
    $leftArrow.setAttribute('style','color:'+$color)

    // for Right Arrow color change
    
    $color=(Math.floor($scrollX.scrollLeft+$scrollX.clientWidth)>$scrollX.scrollWidth-10)? 'unset':'white';
      $RightArrow.setAttribute('style','color:'+$color)
  }